import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import { TabNavigator } from 'react-navigation';
import { Header, Icon } from 'react-native-elements';
import {Constants} from 'expo';

import HomeTab from '../AppTabNavigator/HomeTab';
import SearchTab from '../AppTabNavigator/SearchTab';
import AddMediaTab from '../AppTabNavigator/AddMediaTab';
import LikesTab from '../AppTabNavigator/LikesTab';
import ProfileTab from '../AppTabNavigator/ProfileTab';

const styles = StyleSheet.create({
    statusBar: {
        backgroundColor: '#23B887',
        height: Constants.statusBarHeight
    },
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
});

const AppTabNavigator = TabNavigator({
    HomeTab: {
        screen: HomeTab,
    },
    SearchTab: {
        screen: SearchTab
    },
    AddMediaTab: {
        screen: AddMediaTab
    },
    LikesTab: {
        screen: LikesTab
    },
    ProfileTab: {
        screen: ProfileTab
    }

},{
    animationEnabled: false,
    swipeEnabled: false,
    tabBarPosition: 'bottom',
    tabBarOptions:{
        style:{
            backgroundColor: '#FFFFFF',

        },
        showIcon: true,
        showLabel: false,
        activeTintColor: 'red',
        inactiveTintColor: 'grey'
    }

})

export default class Main extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Header
                    leftComponent={{ icon: 'local-see', color: '#000000' }}
                    centerComponent={{ text: 'Instagram', fontWeight: 'bold', style: { color: '#000000' } }}
                    rightComponent={{ icon: 'near-me', color: '#000000' }}
                    backgroundColor='#FFFFFF'
                >
                </Header>
                <AppTabNavigator />
            </View>

        );
    }
}