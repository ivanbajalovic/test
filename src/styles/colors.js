export default {
    white: '#FFFFFF',
    grey: '#E2E4E3',
    green: '#23B887',
    blueDark: '#003758',
    blueLight: '#00ACEF',
    black: '#000000'
}
