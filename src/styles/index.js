import React from 'react';
import { View, Text } from 'react-native';
import styled from 'styled-components/native';

import Colors from './colors'

export const Row = styled.View`
flex-direction: row;
`
export const Column = styled.View`
flex-direction: column;
`
export const Header = styled.View`
height: 50px;
margin-top: 25px;
padding: 0 15px;
flex-direction: row;
backgroun-color: '#FFFFFF';
justify-content: space-between;
align-items: center;
`