import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Icon } from 'react-native-elements';

export default class AddMediaTab extends Component {

    static navigationOptions = {
        tabBarIcon: ({ tintColor }) =>
            <Icon name='add-box' style={{ color: tintColor }} />
    }
    render() {
        return (
            <View style={styles.container}>
                <Text>AddMediaTab</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})