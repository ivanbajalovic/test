import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, Image } from 'react-native';
import { Icon, Card, Avatar } from 'react-native-elements';

export default class FeedItem extends Component {
    render() {
        return (
            <View >
                <View style={styles.styleView}>
                    <Avatar
                        small
                        rounded
                        source={{ uri: 'https://placeimg.com/640/480/arch' }}
                    />
                    <Text style={styles.styleText}>Petar Petrovic</Text>
                    <Icon iconStyle={{ marginLeft: 200 }}
                        name='more-horiz'
                        onPress={() => alert("Works!")}
                    />
                </View>
                <Image
                    source={require('../images/avocado.jpg')}
                    style={styles.image} />
                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                    <Icon iconStyle={{ marginLeft: 10 }}
                        name='favorite-border' />
                    <Icon iconStyle={{ marginLeft: 15 }}
                        name='question-answer' />
                    <Icon iconStyle={{ marginLeft: 20 }}
                        name='near-me' />
                    <Icon iconStyle={{ marginLeft: 210 }}
                        name='turned-in-not'
                    />
                </View>
                <Text style={styles.styleTextBottom}>
                    101,000 likes
  </Text>
                <Text style={styles.styleTextBottom}>
                    View all coments
  </Text>
            </View >
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    styleView: {
        height: 40,
        marginTop: 10,
        marginLeft: 10,
        flexDirection: 'row',
        alignItems: 'center',
    },
    image: {
        width: 360,
        height: 200
    },
    styleText: {
        textAlignVertical: 'top',
        paddingLeft: 15
    },
    styleTextBottom: {
        marginTop: 15,
        marginBottom: 10,
        marginLeft: 10,
    }
})