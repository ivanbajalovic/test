import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, Image } from 'react-native';
import { Icon, Card, Avatar } from 'react-native-elements';

import FeedItem from './FeedItem';

export default class HomeTab extends Component {

    static navigationOptions = {
        tabBarIcon: ({ tintColor }) =>
            <Icon name='home' style={{ color: tintColor }} />
    }

    render() {
        return (
            <FeedItem />
        )
    }
}