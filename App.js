import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { StackNavigator } from 'react-navigation';

import Main from './src/home/Main'

const AppNavigator = StackNavigator({
  Home: {
    screen: Main
  },

},
  {
    headerMode: 'none'
  })



export default class App extends Component {
  render() {
    return (
      <AppNavigator />
    );
  }
}


